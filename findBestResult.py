import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
import numpy as np
import csv
import argparse
import operator 
from matplotlib import cm
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, classification_report
from csv_writer import Csv_writer

def convert_label(label):
    #in cluster = false alarm, noise = true alarm
    test = label
    test[test != -1] = False
    test[test == -1] = True
    test = test.astype(bool)

    return test

def convert_label_pred(label):
    #in cluster = false alarm, noise = true alarm
    test = label
    test[test == "FALSE"] = False
    test[test == "REJECT"] = True
    test = test.astype(bool)

    return test

def checkClusterform(result):
    for i in result:
        if i >= 0:
            return True
    return False

def main(argv = None):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--data_dir',
        dest='datadir',
        required=True,
        help='Path to the input csv'
    )
    parser.add_argument(
        '--eps_min',
        dest='eps_min',
        default=20,
        type=int,
        help='eps min'
    )
    parser.add_argument(
        '--eps_max',
        dest='eps_max',
        default=220, 
        type=int,
        help='eps max'
    )
    parser.add_argument(
        '--eps_progress',
        dest='eps_progress',
        default=20, 
        type=int,
        help='how much each loop increases'
    )
    parser.add_argument(
        '--minpts_min',
        dest='minpts_min',
        default=5,
        type=int,
        help='minpts min'
    )
    parser.add_argument(
        '--minpts_max',
        dest='minpts_max',
        default=20,
        type=int,
        help='minpts max'
    )
    parser.add_argument(
        '--minpts_progress',
        dest='minpts_progress',
        default=5, 
        type=int,
        help='how much each loop increases'
    )
    known_args, _ = parser.parse_known_args(argv)

    #read csv
    input = pd.read_csv(known_args.datadir, sep = ",", index_col=0)

    #remove X = 0 and Y = 0
    input = input[(input['X'] != 0) & (input['Y'] != 0)]
    input = input.replace("ACCEPT", "FALSE")

    #merge column to find all label defectArea + defectFunction + defectSide
    input['uniqueFilter'] = input['Area'].astype('str') + input['Function'].astype('str') + input['Side']
    input['Prediction'] = np.nan
    input['Prediction_clusterNumber'] = np.nan

    uniqueFilter = np.unique(input['uniqueFilter'].values)
    print(uniqueFilter)
    filename = known_args.datadir.split('.')[0] +'_selectResult.csv'
    noClusterForm = Csv_writer(['label', 'minpts', 'eps'], 'non_formCluster.csv')
    with open(filename, 'w', newline='') as result:
        header = ['label', 'minpts', 'eps', 'precision', 'recall', 'f1', 'isFormCluster', 'totalNum']
        writer = csv.DictWriter(result, fieldnames=header)
        writer.writeheader()
        for i in uniqueFilter:
            print("uniqueFilter", i)
            precision_c = {}
            old_p = -1
            for minpts in range(known_args.minpts_min, known_args.minpts_max, known_args.minpts_progress):
                for eps in range(known_args.eps_min, known_args.eps_max, known_args.eps_progress):
                    # df = input[(input['uniqueFilter'] == i)]
                    df = input.loc[input['uniqueFilter'] == i, ['X', 'Y', 'VerifyType', 'BoardNo']]
                    # print('filter', i)
                    labelLocation = df.index.tolist()
                    
                    X = df[['X', 'Y']]
                    Y = convert_label_pred(df['VerifyType'].values)
                
                    clf = DBSCAN(algorithm='auto', eps=eps, min_samples=minpts)
                    y_pred = clf.fit_predict(X)
                    df['label'] = y_pred

                    #converting label into boolean
                    y_pred_bool = convert_label(df['label'].values)
                    
                    #classification result 
                    classification_result = classification_report(Y, y_pred_bool, output_dict=True)

                    f1 = 0
                    precision = 0
                    recall = 0
                    if checkClusterform(y_pred):
                        formCluster = True
                        precision = classification_result['False']['precision']
                        recall = classification_result['False']['recall']
                        f1 = classification_result['False']['f1-score']
                    else:
                        formCluster = False
                        noClusterForm.writeRow([i, minpts, eps])

                    if old_p < precision:
                        precision_c = {'label': i, 'minpts': minpts, 'eps': eps, 'precision': precision, 'recall' : recall, 'f1': f1, 'isFormCluster' : formCluster, 'totalNum': Y.size}
                        old_p = precision
            writer.writerow(precision_c)
            

if __name__ == "__main__":
    main()
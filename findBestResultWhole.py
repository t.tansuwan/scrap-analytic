import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
import numpy as np
import csv
import argparse
import operator 
from matplotlib import cm
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, classification_report
from csv_writer import Csv_writer
import copy

def convert_label(label):
    #in cluster = false alarm, noise = true alarm
    test = label
    test[test != -1] = False
    test[test == -1] = True
    test = test.astype(bool)

    return test

def convert_label_pred(label):
    #in cluster = false alarm, noise = true alarm
    test = label
    test[test == "FALSE"] = False
    test[test == "REJECT"] = True
    test = test.astype(bool)

    return test

def checkClusterform(result):
    for i in result:
        if i >= 0:
            return True
    return False

def main(argv = None):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--data_dir',
        dest='datadir',
        required=True,
        help='Path to the input csv'
    )
    parser.add_argument(
        '--eps_min',
        dest='eps_min',
        default=20,
        type=int,
        help='eps min'
    )
    parser.add_argument(
        '--eps_max',
        dest='eps_max',
        default=220, 
        type=int,
        help='eps max'
    )
    parser.add_argument(
        '--eps_progress',
        dest='eps_progress',
        default=20, 
        type=int,
        help='how much each loop increases'
    )
    parser.add_argument(
        '--minpts_min',
        dest='minpts_min',
        default=5,
        type=int,
        help='minpts min'
    )
    parser.add_argument(
        '--minpts_max',
        dest='minpts_max',
        default=20,
        type=int,
        help='minpts max'
    )
    parser.add_argument(
        '--minpts_progress',
        dest='minpts_progress',
        default=5, 
        type=int,
        help='how much each loop increases'
    )
    known_args, _ = parser.parse_known_args(argv)

    #read csv
    input = pd.read_csv(known_args.datadir, sep = ",", index_col=0)

    #remove X = 0 and Y = 0
    input = input[(input['X'] != 0) & (input['Y'] != 0)]
    input = input.replace("ACCEPT", "FALSE")

    #merge column to find all label defectArea + defectFunction + defectSide
    input['uniqueFilter'] = input['Area'].astype('str') + input['Function'].astype('str') + input['Side']
    input['Prediction'] = np.nan
    input['Prediction_clusterNumber'] = np.nan

    #visualization test
    x = input['X'].values
    y = input['Y'].values
    plt.scatter(x, y)
    plt.savefig('test.png')

    filename = known_args.datadir.split('.')[0] +'_WholeSelectResult.csv'
    noClusterForm = Csv_writer(['minpts', 'eps'], 'non_formCluster.csv')
    with open(filename, 'w', newline='') as result:
        header = ['minpts', 'eps', 'precision', 'recall', 'f1', 'isFormCluster', 'totalNum']
        writer = csv.DictWriter(result, fieldnames=header)
        writer.writeheader()
        old_p = -1
        Y_pred_Saved = None
        Y_pred_bool_Saved = None
        saved_precision = []
        for minpts in range(known_args.minpts_min, known_args.minpts_max + known_args.minpts_progress, known_args.minpts_progress):
            for eps in range(known_args.eps_min, known_args.eps_max + known_args.eps_progress, known_args.eps_progress):
                X = input[['X', 'Y']]
                Y = convert_label_pred(input['VerifyType'].values)
            
                clf = DBSCAN(algorithm='auto', eps=eps, min_samples=minpts)
                y_pred = clf.fit_predict(X)
                print(y_pred)

                #converting label into boolean
                y_pred_bool = convert_label(copy.deepcopy(y_pred))
                
                #classification result 
                classification_result = classification_report(Y, y_pred_bool, output_dict=True)

                f1 = 0
                precision = 0
                recall = 0
                if checkClusterform(y_pred):
                    formCluster = True
                    precision = classification_result['False']['precision']
                    recall = classification_result['False']['recall']
                    f1 = classification_result['False']['f1-score']
                else:
                    formCluster = False
                    noClusterForm.writeRow([minpts, eps])
                precision_c = {'minpts': minpts, 'eps': eps, 'precision': precision, 'recall' : recall, 'f1': f1, 'isFormCluster' : formCluster, 'totalNum': Y.size}
                writer.writerow(precision_c)

                input['Prediction'] = y_pred_bool
                input['Prediction_clusterNumber'] = y_pred
                input.to_csv("{}minpts{}eps{}_WholePrediction.csv".format(known_args.datadir.split('.')[0], minpts, eps))
            

if __name__ == "__main__":
    main()
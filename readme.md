Scrap scrub dub dub

//if extra arguments options have [], it is optional//

For FindBestResult.py
python findBestResult.py --data_dir data.csv
options:
findBestResult.py --data_dir DATADIR [--eps_min EPS_MIN]
                        [--eps_max EPS_MAX] [--eps_progress EPS_PROGRESS]
                        [--minpts_min MINPTS_MIN] [--minpts_max MINPTS_MAX]
                        [--minpts_progress MINPTS_PROGRESS]

For BestResultWhole.py
python BestResultWhole.py --data_dir data.csv
options:
BestResultWhole.py --data_dir DATADIR [--eps_min EPS_MIN]
                        [--eps_max EPS_MAX] [--eps_progress EPS_PROGRESS]
                        [--minpts_min MINPTS_MIN] [--minpts_max MINPTS_MAX]
                        [--minpts_progress MINPTS_PROGRESS]


Two version of Percentile boundary
1. Cluster the whole data
python PercentileWhole.py --data_dir data.csv --eps 20 --minpts 20 --FilterBySide yes
options:
PercentileWhole.py --data_dir DATADIR
                        [--acceptPercentage ACCEPTPERCENTAGE]
                        [--lowerPercentile LOWERPERCENTILE]
                        [--upperPercentile UPPERPERCENTILE] --eps EPS
                        --minpts MINPTS --FilterBySide FILTERBYSIDE

2. Cluster each combination of area and function
python PercentileFilter.py --data_dir data.csv --keySelection_dir data_selectResult.csv
PercentileFilter.py --data_dir DATADIR --keySelection_dir
                        KEYSELECTION [--acceptPercentage ACCEPTPERCENTAGE]
                        [--lowerPercentile LOWERPERCENTILE]
                        [--upperPercentile UPPERPERCENTILE]

Two version of RGB boundary
1. Cluster the whole data
 py RGBBoundaryWhole.py --data_dir data.csv --eps 20 --minpts 20 --FilterBySide yes
options:
RGBBoundaryWhole.py  --data_dir DATADIR
                        [--acceptPercentage ACCEPTPERCENTAGE] --eps EPS
                        --minpts MINPTS --FilterBySide FILTERBYSIDE

2. Cluster each combination of area and function
python RGBBoundaryFilter.py --data_dir data.csv --keySelection data_selectResult.csv 
RGBBoundaryFilter.py --data_dir DATADIR --keySelection_dir
                        KEYSELECTION [--acceptPercentage ACCEPTPERCENTAGE]
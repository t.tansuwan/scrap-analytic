import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
import numpy as np
import math
import csv
import copy
import argparse
from matplotlib import cm
from csv_writer import Csv_writer
from sklearn.metrics import confusion_matrix, classification_report

#global variables
reject_writer = None
data_input = None
uniqueFilter_writer = None
known_args = None

def clusterChecking(df, cluster_number):
    global data_input
    for num in range(cluster_number):
        cluster = df.loc[df['Cluster_label'] == num]
        numCluster = cluster.shape[0]
        isPass = True
        TrueInBoundary = 0 
        FalseInBoundary = 0 
        TrueOutBoundary = 0
        FalseOutBoundary = 0
        #0 = red 1 = green 2 = blue
        upperbound, lowerbound, std, mean = calUpperLowerBound(cluster['R'].values, cluster['G'].values, cluster['B'].values)
        #check if true alarm is less or exactly 20%
        if isTrueAlarmMorePercentage(cluster, known_args.acceptPercentage/100):
            #if true
            isPass = False
        for index, row in cluster.iterrows():
            R = row['R']
            G = row['G']
            B = row['B']
            #check if in boundary
            if((R <= upperbound[0] and R >= lowerbound[0]) and (G <= upperbound[1] and G >= lowerbound[1]) and (B <= upperbound[2] and B >= lowerbound[2])):
                isInBoundary = False
                #label, clusterNum, 'side', 'boardNo', 'defectNo', 'R', 'G', 'B'
                #True alarm within the boundary
                if row['VerifyType'] == "REJECT":
                    reject_writer.writeRow([num, row['BoardNo'], row['DefectNo'], R, G, B])
                    TrueInBoundary += 1
                    isPass = False
                else:
                    FalseInBoundary += 1
            else:
                isInBoundary = True
                if row['VerifyType'] == "REJECT":
                    TrueOutBoundary += 1
                else:
                    FalseOutBoundary += 1
            data_input.loc[index, 'isInBoundary'] = isInBoundary
        #'AreaText', 'FunctionText', 'side', 'clusterNumber', 'IsPass', 'UpperboundR', 'LowerboundR', 'stdR', 'meanR', 'UpperboundG', 'LowerboundG','stdG', 'meanG', 'UpperboundB', 'LowerboundB','stdB', 'meanB', 'TotalNum', 'TrueInBoundary', 'FalseInBoundary', 'TrueOutBoundary', 'FalseOutBoundary'
        uniqueFilter_writer.writeRow([num, isPass, upperbound[0], lowerbound[0], std[0], mean[0], upperbound[1], lowerbound[1], std[1], mean[1], upperbound[2], lowerbound[2], std[2], mean[2], numCluster, TrueInBoundary, FalseInBoundary, TrueOutBoundary, FalseOutBoundary])

def convert_label(label):
    #in cluster = false alarm, noise = true alarm
    test = copy.deepcopy(label)
    test[test != -1] = False
    test[test == -1] = True
    test = test.astype(bool)

    return test

def isTrueAlarmMorePercentage(defectList, percentage):
    defect_size = defectList.size
    trueAlarm_size = defectList.loc[defectList['VerifyType'] == 'REJECT'].size
    
    if (trueAlarm_size/defect_size) >= percentage:
        return True
    return False

def Filter(filter_input, filter):
    q = filter_input.loc[filter_input['label'] == filter, ['eps', 'minpts']]
    print(q)
    eps = q['eps'].values
    minpts = q['minpts'].values
    print(eps, minpts)
    return eps, minpts

def calUpperLowerBound(r, g ,b):
    colors = [r, g, b]
    upperBound = []
    lowerBound = []
    std_ = []
    mean_ = []
    for color in colors:
        std = np.std(color)
        n = color.size
        mean = np.mean(color)
        upper = mean + 1.695 * (std/ math.sqrt(n))
        lower = mean - 1.695 * (std/ math.sqrt(n))
        upperBound.append(upper)
        lowerBound.append(lower)
        std_.append(std)
        mean_.append(mean)

    return upperBound, lowerBound, std_, mean_

def main(argv = None):
    global known_args, reject_writer, data_input, uniqueFilter_writer
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--data_dir',
        dest='datadir',
        required=True,
        help='Path to the input csv'
    )
    parser.add_argument(
        '--acceptPercentage',
        dest='acceptPercentage',
        type=int,
        default=20,
        help='percentage of how much true alarm can exist in cluster, ex: 20'
    )
    parser.add_argument(
        '--eps',
        dest='eps',
        type=int,
        required=True,
        help='eps for dbscan'
    )
    parser.add_argument(
        '--minpts',
        dest='minpts',
        type=int,
        required=True,
        help='minpts for dbscan'
    )
    parser.add_argument(
        '--FilterBySide',
        dest='filterBySide',
        required=True,
        help='Yes if want to filter by side. If not, then No'
    )
    known_args, _ = parser.parse_known_args(argv)

    #read csv
    data_input = pd.read_csv(known_args.datadir, sep = ",", index_col=0)
    print("data size", data_input.size)

    reject_filename =  "{}minpts{}eps{}_RGBRejectW.csv".format(known_args.datadir.split(".")[0], known_args.minpts, known_args.eps)
    reject_label = ['clusterNum','boardNo', 'defectNo', 'R', 'G', 'B']
    reject_writer = Csv_writer(reject_label, reject_filename)

    #create csv result for each filter
    uniqueFilter_filename = "{}minpts{}eps{}_RGBResultW.csv".format(known_args.datadir.split(".")[0], known_args.minpts, known_args.eps)
    uniqueFilter_label = ['clusterNumber', 'IsPass', 'UpperboundR', 'LowerboundR', 'stdR', 'meanR', 'UpperboundG', 'LowerboundG','stdG', 'meanG', 'UpperboundB', 'LowerboundB','stdB', 'meanB', 'TotalNum', 'TrueInBoundary', 'FalseInBoundary', 'TrueOutBoundary', 'FalseOutBoundary']
    uniqueFilter_writer = Csv_writer(uniqueFilter_label, uniqueFilter_filename)
        
    data_input['Cluster_label'] = np.nan
    data_input['Prediction'] = np.nan
    data_input['isInBoundary'] = np.nan

    if known_args.filterBySide.lower() == 'yes':
        unique_side = data_input.Side.unique()
        for side in unique_side:
            print('side', side)
            uniqueSideCluster = data_input.loc[data_input['Side'] == side]

            X = uniqueSideCluster[['X', 'Y']]
            clf = DBSCAN(algorithm='auto', eps=known_args.eps, min_samples=known_args.minpts)
            y_pred = clf.fit_predict(X)
            y_pred_text = convert_label(copy.deepcopy(y_pred))
            
            uniqueSideCluster['Cluster_label'] = y_pred
            labelLocation = uniqueSideCluster.index.tolist()
            for i in range(len(labelLocation)):
                data_input.loc[labelLocation[i], "Cluster_label"] = y_pred[i]
                data_input.loc[labelLocation[i], "Prediction"] = y_pred_text[i]

            cluster_number = np.amax(y_pred) + 1
            clusterChecking(uniqueSideCluster, cluster_number)
    else:
        X = data_input[['X', 'Y']]

        clf = DBSCAN(algorithm='auto', eps=known_args.eps, min_samples=known_args.minpts)
        y_pred = clf.fit_predict(X)
        y_pred_text = convert_label(copy.deepcopy(y_pred))
        data_input['Cluster_label'] = y_pred
        data_input['Prediction'] = y_pred_text

        cluster_number = np.amax(y_pred) + 1
        clusterChecking(data_input, cluster_number)
    #exporting
    filename = "{}minpts{}eps{}_RGBPredictionW.csv".format(known_args.datadir.split(".")[0], known_args.minpts, known_args.eps)
    print(filename)
    data_input.to_csv(filename)

if __name__ == "__main__":
    main()
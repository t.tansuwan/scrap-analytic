import csv

class Csv_writer:
    def __init__(self, labels, filename):
        self.file = open(filename, "w", newline='')
        self.writer = csv.writer(self.file)
        self.writer.writerow(labels)
    
    def writeRow(self, listItem):
        self.writer.writerow(listItem)
    
    def closeWriter(self):
        self.file.close()
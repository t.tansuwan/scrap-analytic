import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
import numpy as np
import math
import csv
import copy
import argparse
from matplotlib import cm
from csv_writer import Csv_writer
from sklearn.metrics import confusion_matrix, classification_report

def convert_label(label):
    #in cluster = false alarm, noise = true alarm
    test = copy.deepcopy(label)
    test[test != -1] = False
    test[test == -1] = True
    test = test.astype(bool)

    return test

def isTrueAlarmMorePercentage(defectList, percentage):
    defect_size = defectList.size
    trueAlarm_size = defectList.loc[defectList['VerifyType'] == 'REJECT'].size
    
    if (trueAlarm_size/defect_size) >= percentage:
        return True
    return False

def Filter(filter_input, filter):
    q = filter_input.loc[filter_input['label'] == filter, ['eps', 'minpts']]
    print(q)
    eps = q['eps'].values
    minpts = q['minpts'].values
    print(eps, minpts)
    return eps, minpts

def calUpperLowerPer(r, g ,b, lowerPer, upperPer):
    colors = [r, g, b]
    upperBound = []
    lowerBound = []
    for color in colors:
        percentile = np.percentile(color, [lowerPer, upperPer])
        upperBound.append(percentile[1])
        lowerBound.append(percentile[0])

    return upperBound, lowerBound

def main(argv = None):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--data_dir',
        dest='datadir',
        required=True,
        help='Path to the input csv'
    )
    parser.add_argument(
        '--keySelection_dir',
        dest='keySelection',
        required=True,
        help='Eps number for dbscan'
    )
    parser.add_argument(
        '--acceptPercentage',
        dest='acceptPercentage',
        type=int,
        default=20,
        help='percentage of how much true alarm can exist in cluster, ex: 20'
    )
    parser.add_argument(
        '--lowerPercentile',
        dest='lowerPercentile',
        type=int,
        default=30,
        help='lower bound percentile'
    )
    parser.add_argument(
        '--upperPercentile',
        dest='upperPercentile',
        type=int,
        default=70,
        help='upper bound percentile'
    )
    known_args, _ = parser.parse_known_args(argv)

    #read csv
    input = pd.read_csv(known_args.datadir, sep = ",", index_col=0)
    filter_input = pd.read_csv(known_args.keySelection, sep=',')
    print(filter_input)

    print("data size", input.size)

    #merge column to find all label defectArea + defectFunction + defectSide
    input['uniqueFilter'] = input['Area'].astype('str') + input['Function'].astype('str') + input['Side']
    input['Prediction'] = np.nan
    input['Cluster_label'] = np.nan
    input['isInBoundary'] = np.nan

    #find all unique filter
    # uniqueFilter = np.unique(input['uniqueFilter'].values)
    uniqueFilter = filter_input.loc[filter_input['isFormCluster'] == True]
    uniqueFilter = uniqueFilter['label'].values

    reject_filename = known_args.datadir.split(".")[0] + "_PerRejectF.csv"
    reject_label = ['AreaText', 'FunctionText', 'side', 'clusterNum','boardNo', 'defectNo', 'R', 'G', 'B']
    reject_writer = Csv_writer(reject_label, reject_filename)

    #create csv result for each filter
    uniqueFilter_filename = known_args.datadir.split(".")[0] + "_PerResultF.csv"
    uniqueFilter_label = ['AreaText', 'FunctionText', 'side', 'clusterNumber', 'IsPass', 'UpperPerR', 'LowerPerR', 'UpperPerG', 'LowerPerG', 'UpperPerB', 'LowerPerB','TotalNum', 'TrueInBoundary', 'FalseInBoundary', 'TrueOutBoundary', 'FalseOutBoundary']
    uniqueFilter_writer = Csv_writer(uniqueFilter_label, uniqueFilter_filename)
    print(uniqueFilter)
    counter = 0
    for filter in uniqueFilter:
        df = input.loc[input['uniqueFilter'] == filter, ['BoardNo','Side', 'DefectNo', 'X', 'Y', 'VerifyType', 'R', 'G', 'B', 'AreaText', 'FunctionText']]
        area = df.AreaText.unique()[0]
        function = df.FunctionText.unique()[0]
        side = df.Side.unique()[0]
        print(area, function)
        labelLocation = df.index.tolist()
        
        X = df[['X', 'Y']]

        eps, minpts = Filter(filter_input, filter)

        clf = DBSCAN(algorithm='auto', eps=eps, min_samples=minpts)
        y_pred = clf.fit_predict(X)
        
        cluster_number = np.amax(y_pred) + 1
        df['Cluster_label'] = y_pred
        
        y_pred_text = convert_label(y_pred)

        #adding prediction result to the main input
        for i in range(len(labelLocation)):
            input.loc[labelLocation[i], "Cluster_label"] = y_pred[i]
            input.loc[labelLocation[i], "Prediction"] = y_pred_text[i]
            
        #loop through each cluster created from DBScan and locate only True alarm and check if it's over the boundary or not
        for num in range(cluster_number):
            counter += 1
            cluster = df.loc[df['Cluster_label'] == num]
            numCluster = cluster.shape[0]
            isPass = True
            TrueInBoundary = 0 
            FalseInBoundary = 0 
            TrueOutBoundary = 0
            FalseOutBoundary = 0
            #0 = red 1 = green 2 = blue
            upperbound, lowerbound = calUpperLowerPer(cluster['R'].values, cluster['G'].values, cluster['B'].values, known_args.lowerPercentile, known_args.upperPercentile)
            #check if true alarm is less or exactly 20%
            if isTrueAlarmMorePercentage(cluster, known_args.acceptPercentage/100):
                #if true
                isPass = False
            # cluster_trueAlarm = cluster.loc[df['VerifyType'] == "REJECT"]
            for index, row in cluster.iterrows():
                R = row['R']
                G = row['G']
                B = row['B']
                #check if in boundary
                if((R <= upperbound[0] and R >= lowerbound[0]) and (G <= upperbound[1] and G >= lowerbound[1]) and (B <= upperbound[2] and B >= lowerbound[2])):
                    isInBoundary = True
                    #label, clusterNum, 'side', 'boardNo', 'defectNo', 'R', 'G', 'B'
                    #True alarm within the boundary
                    if row['VerifyType'] == "REJECT":
                        reject_writer.writeRow([area, function, row['Side'], num, row['BoardNo'], row['DefectNo'], R, G, B])
                        TrueInBoundary += 1
                        isPass = False
                    else:
                        FalseInBoundary += 1
                else:
                    isInBoundary = False
                    if row['VerifyType'] == "REJECT":
                        TrueOutBoundary += 1
                    else:
                        FalseOutBoundary += 1
                input.loc[index, 'isInBoundary'] = isInBoundary
            #'AreaText', 'FunctionText', 'side', 'clusterNumber', 'IsPass', 'UpperboundR', 'LowerboundR','UpperboundG', 'LowerboundG', 'UpperboundB', 'LowerboundB','totalNum', 'TrueInBoundary', 'FalseInBoundary', 'TrueOutBoundary', 'FalseOutBoundary'
            uniqueFilter_writer.writeRow([area, function, side, num, isPass, upperbound[0], lowerbound[0], upperbound[1], lowerbound[1], upperbound[2], lowerbound[2], numCluster, TrueInBoundary, FalseInBoundary, TrueOutBoundary, FalseOutBoundary])
    #exporting
    input = input.drop(columns=['uniqueFilter'])
    print(input)
    filename = known_args.datadir.split(".")[0] + "_PerPredictionF.csv"
    print(filename)
    input.to_csv(filename)

if __name__ == "__main__":
    main()
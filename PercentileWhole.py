import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
import numpy as np
import math
import csv
import copy
import argparse
from matplotlib import cm
from csv_writer import Csv_writer
from sklearn.metrics import confusion_matrix, classification_report

#global variable
reject_writer = None
data_input = None
uniqueFilter_writer = None
known_args = None

def clusterChecking(df, cluster_number):
    global data_input
    for num in range(cluster_number):
        print('cluster number', num)
        cluster = df.loc[df['Cluster_label'] == num]
        numCluster = cluster.shape[0]
        isPass = True
        TrueInBoundary = 0 
        FalseInBoundary = 0 
        TrueOutBoundary = 0
        FalseOutBoundary = 0
        
        upperbound, lowerbound = calUpperLowerPer(cluster['R'].values, cluster['G'].values, cluster['B'].values, known_args.lowerPercentile, known_args.upperPercentile)
        if isTrueAlarmMorePercentage(cluster, known_args.acceptPercentage/100):
            #if true
            isPass = False
        for index, row in cluster.iterrows():
            R = row['R']
            G = row['G']
            B = row['B']
            #check if in boundary
            if((R <= upperbound[0] and R >= lowerbound[0]) and (G <= upperbound[1] and G >= lowerbound[1]) and (B <= upperbound[2] and B >= lowerbound[2])):
                #label, clusterNum, 'side', 'boardNo', 'defectNo', 'R', 'G', 'B'
                #True alarm within the boundary
                isInBoundary = True
                if row['VerifyType'] == "REJECT":
                    reject_writer.writeRow([row['Side'], num, row['BoardNo'], row['DefectNo'], R, G, B])
                    TrueInBoundary += 1
                    isPass = False
                else:
                    FalseInBoundary += 1
            else:
                isInBoundary = False
                if row['VerifyType'] == "REJECT":
                    TrueOutBoundary += 1
                else:
                    FalseOutBoundary += 1
            data_input.loc[index, 'isInBoundary'] = isInBoundary
        uniqueFilter_writer.writeRow([row['Side'], num, isPass, upperbound[0], lowerbound[0], upperbound[1], lowerbound[1], upperbound[2], lowerbound[2], numCluster, TrueInBoundary, FalseInBoundary, TrueOutBoundary, FalseOutBoundary])

def convert_label(label):
    #in cluster = false alarm, noise = true alarm
    test = copy.deepcopy(label)
    test[test != -1] = False
    test[test == -1] = True
    test = test.astype(bool)

    return test

def isTrueAlarmMorePercentage(defectList, percentage):
    defect_size = defectList.size
    trueAlarm_size = defectList.loc[defectList['VerifyType'] == 'REJECT'].size
    
    if (trueAlarm_size/defect_size) >= percentage:
        return True
    return False 

def calUpperLowerPer(r, g ,b, lowerPer, upperPer):
    colors = [r, g, b]
    upperBound = []
    lowerBound = []
    for color in colors:
        percentile = np.percentile(color, [lowerPer, upperPer])
        upperBound.append(percentile[1])
        lowerBound.append(percentile[0])

    return upperBound, lowerBound

def main(argv = None):
    global known_args, reject_writer, data_input, uniqueFilter_writer

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--data_dir',
        dest='datadir',
        required=True,
        help='Path to the data_input csv'
    )
    parser.add_argument(
        '--acceptPercentage',
        dest='acceptPercentage',
        type=int,
        default=20,
        help='percentage of how much true alarm can exist in cluster, ex: 20'
    )
    parser.add_argument(
        '--lowerPercentile',
        dest='lowerPercentile',
        type=int,
        default=30,
        help='lower bound percentile'
    )
    parser.add_argument(
        '--upperPercentile',
        dest='upperPercentile',
        type=int,
        default=70,
        help='upper bound percentile'
    )
    parser.add_argument(
        '--eps',
        dest='eps',
        required=True,
        type=int,
        help='eps'
    )
    parser.add_argument(
        '--minpts',
        dest='minpts',
        required=True,
        type=int,
        help='minpts'
    )
    parser.add_argument(
        '--FilterBySide',
        dest='filterBySide',
        required=True,
        help='yes if want to filter by side. If not, then no'
    )
    known_args, _ = parser.parse_known_args(argv)

    #read csv
    data_input = pd.read_csv(known_args.datadir, sep = ",", index_col=0)
    print(data_input)
    print("data size", data_input.shape)

    reject_filename = "{}minpts{}eps{}_PerRejectW.csv".format(known_args.datadir.split(".")[0], known_args.minpts, known_args.eps)
    reject_label = ['side', 'clusterNum','boardNo', 'defectNo', 'R', 'G', 'B']
    reject_writer = Csv_writer(reject_label, reject_filename)

    #create csv result for each filter
    uniqueFilter_filename = "{}minpts{}eps{}_PerResultW.csv".format(known_args.datadir.split(".")[0], known_args.minpts, known_args.eps)
    uniqueFilter_label = ['side', 'clusterNumber', 'IsPass', 'UpperPerR', 'LowerPerR', 'UpperPerG', 'LowerPerG', 'UpperPerB', 'LowerPerB','TotalNum', 'TrueInBoundary', 'FalseInBoundary', 'TrueOutBoundary', 'FalseOutBoundary']
    uniqueFilter_writer = Csv_writer(uniqueFilter_label, uniqueFilter_filename)
    
    data_input['Cluster_label'] = np.nan
    data_input['Prediction'] = np.nan
    data_input['isInBoundary'] = np.nan

    #loop through each cluster created from DBScan and locate only True alarm and check if it's over the boundary or not
    if known_args.filterBySide.lower() == 'yes':
        unique_side = data_input.Side.unique()
        for side in unique_side:
            print('side', side)
            uniqueSideCluster = data_input.loc[data_input['Side'] == side]

            X = uniqueSideCluster[['X', 'Y']]
            clf = DBSCAN(algorithm='auto', eps=known_args.eps, min_samples=known_args.minpts)
            y_pred = clf.fit_predict(X)
            y_pred_text = convert_label(copy.deepcopy(y_pred))
            
            uniqueSideCluster['Cluster_label'] = y_pred
            labelLocation = uniqueSideCluster.index.tolist()
            for i in range(len(labelLocation)):
                data_input.loc[labelLocation[i], "Cluster_label"] = y_pred[i]
                data_input.loc[labelLocation[i], "Prediction"] = y_pred_text[i]

            cluster_number = np.amax(y_pred) + 1
            clusterChecking(uniqueSideCluster, cluster_number)
    else:
        X = data_input[['X', 'Y']]

        clf = DBSCAN(algorithm='auto', eps=known_args.eps, min_samples=known_args.minpts)
        y_pred = clf.fit_predict(X)
        y_pred_text = convert_label(copy.deepcopy(y_pred))
        data_input['Cluster_label'] = y_pred
        data_input['Prediction'] = y_pred_text

        cluster_number = np.amax(y_pred) + 1
        clusterChecking(data_input, cluster_number)
    #exporting
    filename = "{}minpts{}eps{}_PerPredictionW.csv".format(known_args.datadir.split(".")[0], known_args.minpts, known_args.eps)
    print(filename)
    data_input.to_csv(filename)

if __name__ == "__main__":
    main()